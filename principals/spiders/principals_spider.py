import scrapy
from ..items import PrincipalsItem


class PrincipalsSpider(scrapy.Spider):
    name = "principals"
    find_by_country_url = [
        "https://efile.fara.gov/ords/f?p=185:130:",
        "::NO::P130_CNTRY:",
    ]
    start_urls = [
        "https://efile.fara.gov/ords/f?p=185:130:::NO:RP,130:P130_DATERANGE:N"
    ]

    def parse(self, response):
        options = response.css(
            ".apex-item-select option::attr(value)").extract()
        del options[0]
        salt = response.css('input[name="p_instance"]::attr(value)').extract()

        for country in options:
            next_page = (
                PrincipalsSpider.find_by_country_url[0]
                + str(salt[0])
                + PrincipalsSpider.find_by_country_url[1]
                + str(country)
            )
            yield response.follow(next_page, callback=self.parse_country_page)

    def parse_country_page(self, response):
        country_name = response.css(".a-IRR-header--group::text").extract()
        url_domain = "https://efile.fara.gov/pls/apex/"
        principals = response.css(".a-IRR-table tr")[2:]

        if len(principals) == 0:
            return

        for principal in principals:
            url = (
                url_domain
                + principal.css(
                    'td[headers="LINK B555216666378934863_1"] a::attr(href)'
                ).extract()[0]
            )
            state = principal.css(
                'td[headers="STATE B555216666378934863_1"]::text'
            ).extract()
            reg_num = principal.css(
                'td[headers="REG_NUMBER B555216666378934863_1"]::text'
            ).extract()
            address = principal.css(
                'td[headers="ADDRESS_1 B555216666378934863_1"]::text'
            ).extract()
            foreign_principal = principal.css(
                'td[headers="FP_NAME B555216666378934863_1"]::text'
            ).extract()
            date = principal.css(
                'td[headers="FP_REG_DATE B555216666378934863_1"]::text'
            ).extract()
            registrant = principal.css(
                'td[headers="REGISTRANT_NAME B555216666378934863_1"]::text'
            ).extract()

            item = PrincipalsItem()

            item["url"] = url
            item["country"] = country_name
            item["state"] = state
            item["reg_num"] = reg_num
            item["address"] = address
            item["foreign_principal"] = foreign_principal
            item["date"] = date
            item["registrant"] = registrant

            request = scrapy.Request(
                item["url"], callback=self.parse_exhibit_url, dont_filter=True
            )
            request.meta["item"] = item
            yield request

    def parse_exhibit_url(self, response):
        item = response.meta.get("item")
        item["exhibit_url"] = []
        item_names = response.css(
            'td[headers="DOCLINK"] a span::text').extract()
        exhibit_urls = response.css(
            'td[headers="DOCLINK"] a::attr(href)').extract()
        i = 0

        for name in item_names:

            name = name.rstrip().title()
            item_foreign_principal = item["foreign_principal"][0].rstrip(
            ).title()

            if name == item_foreign_principal:
                item["exhibit_url"].append(exhibit_urls[i])
                i += 1
            else:
                i += 1

        yield item
