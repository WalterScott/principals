# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class PrincipalsPipeline(object):
    def process_item(self, item, spider):
        item["country"] = item["country"][0].replace(
            "Country/Location Represented : ", ""
        )
        item["state"] = item["state"][0] if (len(item["state"])) else None
        item["reg_num"] = item["reg_num"][0] if (
            len(item["reg_num"])) else None
        item["date"] = item["date"][0] if (len(item["date"])) else None
        item["address"] = (
            " ".join(item["address"]).replace(u"\u00A0", " ").rstrip()
            if (len(item["address"]) > 1)
            else None
        )
        item["foreign_principal"] = (
            " ".join(item["foreign_principal"]) if (
                len(item["registrant"])) else None
        )
        item["registrant"] = (
            " ".join(item["registrant"]) if (len(item["registrant"])) else None
        )
        item["exhibit_url"] = (
            item["exhibit_url"][0]
            if (len(item["exhibit_url"]) == 1)
            else item["exhibit_url"]
            if (len(item["exhibit_url"]))
            else None
        )
        return item
